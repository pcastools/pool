// Pool implements a pool with expiry.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package pool

import (
	"container/list"
	"errors"
	"fmt"
	"sync"
	"time"
)

// poolElement describes an element in the pool.
type poolElement struct {
	Value   interface{} // The value
	Expires time.Time   // The expiry time
}

// elementPool is a pool of *poolElement objects available for reuse.
var elementPool = &sync.Pool{
	New: func() interface{} { return &poolElement{} },
}

// Pool maintains a pool of elements available for reuse.
type Pool struct {
	newFunc     func() (interface{}, error) // Create a new item
	expiredFunc func(interface{})           // Called on an expired item
	lifetime    time.Duration               // The lifetime of items in the pool
	wakeC       chan<- struct{}             // Wake the worker
	doneC       chan<- struct{}             // Close to ask the worker to exit
	m           sync.Mutex                  // Controls access to the following
	isClosed    bool                        // Have we closed?
	l           *list.List                  // The available clients
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// newElement returns a new *poolElement initialised with the given value and expiry time.
func newElement(value interface{}, expires time.Time) *poolElement {
	elt := elementPool.Get().(*poolElement)
	elt.Value = value
	elt.Expires = expires
	return elt
}

// reuseElement makes the given *poolElement available for reuse. Note: This will set the Value equal to nil, so the caller doesn't need to do this.
func reuseElement(elt *poolElement) {
	if elt != nil {
		elt.Value = nil
		elementPool.Put(elt)
	}
}

// execNewFunc executes the give new function f, recovering from any panics.
func execNewFunc(f func() (interface{}, error)) (value interface{}, err error) {
	// Recover from any panics caused by the user's new function f
	defer func() {
		if e := recover(); e != nil {
			err = fmt.Errorf("Panic in new func: %s", e)
		}
	}()
	// Execute the new function
	value, err = f()
	return
}

// execExpiredFunc executes the given expired function f on the given value, recovering from any panics.
func execExpiredFunc(f func(interface{}), value interface{}) {
	if f != nil {
		defer recover()
		f(value)
	}
}

/////////////////////////////////////////////////////////////////////////
// Pool functions
/////////////////////////////////////////////////////////////////////////

// New returns a new pool, where items in the pool have the given lifetime. The function newFunc will be called to create a new item when the pool is empty; the function expiredFunc will be called when an item in the pool has expired, and may be nil.
func New(lifetime time.Duration, newFunc func() (interface{}, error), expiredFunc func(interface{})) *Pool {
	// Sanity check
	if newFunc == nil {
		panic("The newFunc must not be nil")
	}
	// Create the doneC channels
	wakeC, doneC := make(chan struct{}, 1), make(chan struct{})
	// Create the pool
	p := &Pool{
		newFunc:     newFunc,
		expiredFunc: expiredFunc,
		lifetime:    lifetime,
		wakeC:       wakeC,
		doneC:       doneC,
	}
	// Start the background worker and return the pool
	go p.poolWorker(wakeC, doneC)
	return p
}

// poolWorker is the worker that removes expired elements from the pool. Close the doneC channel to ask the worker to exit. Intended to be run in its own go-routine.
func (p *Pool) poolWorker(wakeC <-chan struct{}, doneC <-chan struct{}) {
	var t *time.Timer
	for {
		// How we proceed depends on whether the timer is defined or not
		if t == nil {
			select {
			case <-wakeC:
			case <-doneC:
				return // We've been asked to exit
			}
		} else {
			select {
			case <-wakeC:
				t.Stop()
			case <-doneC:
				t.Stop()
				return // We've been asked to exit
			case <-t.C:
				// Remove any expired elements from the pool
				p.prune()
			}
		}
		// Acquire a lock
		p.m.Lock()
		// Remove any wakeC message that might be in the channel buffer
		select {
		case <-wakeC:
		default:
		}
		// Reset the timer
		if p.l == nil {
			t = nil
		} else if e := p.l.Front(); e == nil {
			t = nil
		} else {
			d := time.Until(e.Value.(*poolElement).Expires)
			if t == nil {
				t = time.NewTimer(d)
			} else {
				t.Reset(d)
			}
		}
		// Release the lock
		p.m.Unlock()
	}
}

// prune removes any expired elements from the pool.
func (p *Pool) prune() {
	// Acquire a lock
	p.m.Lock()
	// Make a note of the current time
	now := time.Now()
	// Walk the list, which we assume is in increasing expiry time order
	var expired []*poolElement
	done := false
	if l := p.l; l != nil {
		for e := l.Front(); e != nil && !done; e = l.Front() {
			// Has this element expired?
			elt := e.Value.(*poolElement)
			if done = elt.Expires.After(now); !done {
				// Note the element and remove it from the list
				if expired == nil {
					expired = make([]*poolElement, 0, 3)
				}
				expired = append(expired, elt)
				l.Remove(e)
			}
		}
	}
	// Release the lock
	p.m.Unlock()
	// Walk through the expired items, calling the expired function on each one
	for _, elt := range expired {
		execExpiredFunc(p.expiredFunc, elt.Value)
		reuseElement(elt)
	}
}

// Close closes the pool, expiring any items currently in the pool and preventing new items from being created.
func (p *Pool) Close() error {
	// Sanity check
	if p == nil {
		return nil
	}
	// Acquire a lock
	p.m.Lock()
	// Is there anything to do?
	var l *list.List
	if !p.isClosed {
		// Stop the background worker and mark us as closed
		close(p.doneC)
		p.isClosed = true
		// Make a note of the list and clear it
		l, p.l = p.l, nil
	}
	// Release the lock
	p.m.Unlock()
	// Expire any items in the list
	if l != nil {
		for e := l.Front(); e != nil; e = e.Next() {
			elt := e.Value.(*poolElement)
			execExpiredFunc(p.expiredFunc, elt.Value)
			reuseElement(elt)
		}
	}
	return nil
}

// Get returns a new item from the pool.
func (p *Pool) Get() (interface{}, error) {
	// Sanity check
	if p == nil || p.newFunc == nil {
		return nil, errors.New("Uninitialised pool")
	}
	// Acquire a lock
	p.m.Lock()
	// Have we closed?
	if p.isClosed {
		p.m.Unlock()
		return nil, errors.New("Pool closed")
	}
	// Is there an item in the list?
	var elt *poolElement
	if p.l != nil && p.l.Len() != 0 {
		elt = p.l.Remove(p.l.Front()).(*poolElement)
		// Ask the worker to wake from sleep
		select {
		case p.wakeC <- struct{}{}:
		default:
		}
	}
	// Release the lock on the list
	p.m.Unlock()
	// Did we get an item from the list? Otherwise create a new item.
	if elt != nil {
		value := elt.Value
		reuseElement(elt)
		return value, nil
	}
	return execNewFunc(p.newFunc)
}

// Put places the given item in the pool.
func (p *Pool) Put(x interface{}) {
	// Sanity check
	if p == nil || p.newFunc == nil {
		panic("Uninitialised pool")
	}
	// Acquire a lock
	p.m.Lock()
	// Have we closed?
	if p.isClosed {
		p.m.Unlock()
		execExpiredFunc(p.expiredFunc, x)
		return
	}
	// If the list doesn't exist, create it
	if p.l == nil {
		p.l = list.New()
	}
	// Append the item to the end of the list
	p.l.PushBack(newElement(x, time.Now().Add(p.lifetime)))
	if p.l.Len() == 1 {
		// Ask the worker to wake from sleep
		select {
		case p.wakeC <- struct{}{}:
		default:
		}
	}
	// Release the lock on the list
	p.m.Unlock()
}
