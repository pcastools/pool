// Tests for pool.go

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package pool

import (
	"testing"
	"time"
)

func TestPool(t *testing.T) {
	// Create a new pool
	count := 0
	p := New(time.Second, func() (interface{}, error) {
		v := count
		count++
		return v, nil
	}, func(_ interface{}) {})
	// Start fetching and adding elements to the pool
	v1, err := p.Get()
	if err != nil {
		t.Error(err)
	}
	v2, err := p.Get()
	if err != nil {
		t.Error(err)
	}
	v3, err := p.Get()
	if err != nil {
		t.Error(err)
	}
	p.Put(v1)
	time.Sleep(time.Second / 2)
	p.Put(v2)
	v1, err = p.Get()
	if err != nil {
		t.Error(err)
	}
	time.Sleep(time.Second / 2)
	p.Put(v1)
	time.Sleep(time.Second)
	p.Put(v3)
	// Close the pool
	p.Close()
}
